#!/bin/bash

set -euox pipefail

export ANSIBLE_CALLBACKS_ENABLED=profile_tasks
export ANSIBLE_FORCE_COLOR=true

ansible-playbook -i '127.0.0.1,' laptop.yml --vault-password-file ./ci/scripts/getpass.sh
bats ./ci/test/
