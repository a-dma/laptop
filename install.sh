#!/bin/bash

DEFINED_TAGS=$(ansible-playbook laptop.yml --list-tags 2>/dev/null |\
                  grep "TASK TAGS" |\
                  sed -n -e 's/^.*\[\(.*\)\]/\1/p' |\
                  tr -d ' ')

function usage {
  echo -e "run \`$0 TAG1,TAG2,...,TAGn\` to execute specific tasks"
  echo -e "or \`$0\` to execute all tasks"
  echo -e "Available tags are:\n\t$DEFINED_TAGS\n"
  echo -e "-h, --help\tprint this help and exit"
  echo -e "-d, --dry-run\texecute the playbook in check mode (some tasks are skipped)"
}

while [ $# -gt 0 ]
do
  key="$1"

  case $key in
    -h|--help)
      usage
      exit 0
      ;;

    -d|--dry-run)
      DRY_RUN="--check"
      ;;

    *)
      # NOTE(adma): Quick and dirty check to see if a tag exists
      # it's not a bullet-proof solution, but good enough
      if test "${DEFINED_TAGS#*$key}" != "$DEFINED_TAGS"
      then
        TARGET_TAGS="${TARGET_TAGS}${key},"
      else
        echo undefined tag $key
        exit 1
      fi
      ;;
  esac
  shift # past argument or value
done

if [ "$TARGET_TAGS" == "" ];
then
  TARGET_TAGS="$DEFINED_TAGS"
fi

echo "Using tags: $TARGET_TAGS"

ansible-playbook -i '127.0.0.1,' laptop.yml $DRY_RUN -t $TARGET_TAGS --ask-become-pass --ask-vault-pass
