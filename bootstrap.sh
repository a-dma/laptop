#!/bin/bash

function vercomp () {
  if [[ $1 == $2 ]]
  then
    return 0
  fi
  local IFS=.
  local i ver1=($1) ver2=($2)
  # fill empty fields in ver1 with zeros
  for ((i=${#ver1[@]}; i<${#ver2[@]}; i++))
  do
    ver1[i]=0
  done
  for ((i=0; i<${#ver1[@]}; i++))
  do
    if [[ -z ${ver2[i]} ]]
    then
      # fill empty fields in ver2 with zeros
      ver2[i]=0
    fi
    if ((10#${ver1[i]} > 10#${ver2[i]}))
    then
      return 1
    fi
    if ((10#${ver1[i]} < 10#${ver2[i]}))
    then
      return 2
    fi
  done
  return 0
}

echo -n "Checking for ansible ... "
if [ $(dpkg-query -W -f='${Status}' ansible 2>/dev/null | grep -c "ok installed") -eq 0 ];
then
  echo "installing"
  sudo apt install ansible
else
  echo "already installed, skipping"
fi

echo -n "Checking ansible version ... "
min_ansible_version=2.1.0.0
ansible_version=$(ansible --version | head -n 1 | sed -n -e 's/^ansible \(.*\)$/\1/p')
vercomp "$ansible_version" "$min_ansible_version"
if [ $? -eq 2 ];
then
  cat <<EOF
ansible version >= $min_ansible_version is required.
Version $ansible_version was found through your package manager.
Install a newer version.
A possible solution is to get ansible from the PPA:
sudo add-apt-repository ppa:ansible/ansible
sudo apt update
sudo apt upgrade ansible

Rerun this script afterwards.
EOF
  exit 1
else
  echo "found version >= $min_ansible_version ($ansible_version)"
fi

exit 0
