#!/usr/bin/env bats

@test "Ensure automake binary is found in PATH" {
  run which automake
  [ "$status" -eq 0 ]
}

@test "Ensure clang binary is found in PATH" {
  run which clang
  [ "$status" -eq 0 ]
}

@test "Ensure clang-format binary is found in PATH" {
  run which clang-format
  [ "$status" -eq 0 ]
}

@test "Ensure cmake binary is found in PATH" {
  run which cmake
  [ "$status" -eq 0 ]
}

@test "Ensure gcc binary is found in PATH" {
  run which gcc
  [ "$status" -eq 0 ]
}

@test "Ensure gdb binary is found in PATH" {
  run which gdb
  [ "$status" -eq 0 ]
}

@test "Ensure gengetopt binary is found in PATH" {
  run which gengetopt
  [ "$status" -eq 0 ]
}

@test "Ensure help2man binary is found in PATH" {
  run which help2man
  [ "$status" -eq 0 ]
}

@test "Ensure libtool binary is found in PATH" {
  run which libtoolize
  [ "$status" -eq 0 ]
}

@test "Ensure make binary is found in PATH" {
  run which make
  [ "$status" -eq 0 ]
}

@test "Ensure ninja binary is found in PATH" {
  run which ninja
  [ "$status" -eq 0 ]
}

@test "Ensure pkg-config binary is found in PATH" {
  run which pkg-config
  [ "$status" -eq 0 ]
}
