#!/usr/bin/env bats

@test "Ensure curl binary is found in PATH" {
  run which curl
  [ "$status" -eq 0 ]
}

@test "Ensure emacs binary is found in PATH" {
  run which emacs
  [ "$status" -eq 0 ]
}

@test "Ensure git binary is found in PATH" {
  run which git
  [ "$status" -eq 0 ]
}

@test "Ensure keepassxc binary is found in PATH" {
  run which keepassxc
  [ "$status" -eq 0 ]
}

@test "Ensure stow binary is found in PATH" {
  run which stow
  [ "$status" -eq 0 ]
}

@test "Ensure tmux binary is found in PATH" {
  run which tmux
  [ "$status" -eq 0 ]
}

@test "Ensure vim binary is found in PATH" {
  run which vim
  [ "$status" -eq 0 ]
}

@test "Ensure xclip binary is found in PATH" {
  run which xclip
  [ "$status" -eq 0 ]
}
