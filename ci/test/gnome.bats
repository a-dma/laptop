#!/usr/bin/env bats

EXT_DIR="$HOME/.local/share/gnome-shell/extensions"

@test "Ensure guake binary is found in PATH" {
  run which guake
  [ "$status" -eq 0 ]
}

@test "Ensure gnome-tweaks binary is found in PATH" {
  run which gnome-tweaks
  [ "$status" -eq 0 ]
}

@test "Ensure gsettings binary is found in PATH" {
  run which gsettings
  [ "$status" -eq 0 ]
}

@test "Ensure GNOME Shell extensions are enabled" {
  SCHEMA_NAME="org.gnome.shell"
  EXTENSIONS=(
    apt-update-indicator@franglais125.gmail.com
    refresh-wifi@kgshank.net
    unite@hardpixel.eu
  )

  run gsettings get org.gnome.shell enabled-extensions

  for i in $(seq 0 $((${#EXTENSIONS[@]} - 1)));
  do
    [[ $output = *"${EXTENSIONS[$i]}"* ]];
  done
}

@test "Check settings for GNOME Shell extension \"apt-update-indicator\"" {
  EXT_NAME="apt-update-indicator@franglais125.gmail.com"
  SCHEMA_NAME="org.gnome.shell.extensions.apt-update-indicator"
  KEYS=(
    check-interval
    notify
    output-on-terminal
    check-cmd-custom
    residual-packages
    shortcut-text
    obsolete-packages
    show-count
    transient
    terminal
    strip-versions
    update-cmd
    use-custom-cmd
    show-critical-updates
    update-cmd-options
    new-packages
    apt-update-indicator-shortcut
    always-visible
    auto-expand-list
    ignore-list
    autoremovable-packages
    use-shortcut
    interval-unit
    verbosity
  )
  VALUES=(
    1
    false
    false
    \'pkexec\ apt\ update\'
    true
    \'\<Super\>i\'
    true
    true
    true
    \'gnome-terminal\ -x\ bash\ -c\'
    true
    \'pkexec\ apt\ upgrade\ -y\'
    false
    true
    \'gnome-software\'
    true
    \[\'\<Super\>i\'\]
    false
    0
    \'\'
    true
    false
    \'days\'
    0
  )

  for i in $(seq 0 $((${#KEYS[@]} - 1)));
  do
    run gsettings --schemadir $EXT_DIR/$EXT_NAME/schemas get $SCHEMA_NAME ${KEYS[$i]}
    [ "$output" = "${VALUES[$i]}" ];
  done
}

@test "Check settings for GNOME Shell extension \"unite\"" {
  EXT_NAME="unite@hardpixel.eu"
  SCHEMA_NAME="org.gnome.shell.extensions.unite"
  KEYS=(
    extend-left-box
    hide-dropdown-arrows
    use-system-fonts
    show-window-buttons
#    desktop-name-text
    window-buttons-theme
    show-legacy-tray
    notifications-position
    hide-window-titlebars
    hide-aggregate-menu-arrow
    show-desktop-name
    window-buttons-placement
    hide-app-menu-arrow
    hide-activities-button
    greyscale-tray-icons
    show-window-title
    hide-app-menu-icon
    restrict-to-primary-screen
    reduce-panel-spacing
  )
  VALUES=(
     false
     true
     true
     \'maximized\'
#     \'GNOME Desktop\' The space here seems to create problems?
     \'default-dark\'
     true
     \'center\'
     \'maximized\'
     false
     true
     \'auto\'
     true
     \'auto\'
     false
     \'maximized\'
     true
     false
     true
  )

  for i in $(seq 0 $((${#KEYS[@]} - 1)));
  do
    run gsettings --schemadir $EXT_DIR/$EXT_NAME/schemas get $SCHEMA_NAME ${KEYS[$i]}
    [ "$output" = "${VALUES[$i]}" ];
  done
}

@test "Check enabled battery percentage on top bar" {
  run gsettings get org.gnome.desktop.interface show-battery-percentage
  [ "$output" = "true" ];
}
