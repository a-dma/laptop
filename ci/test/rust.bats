#!/usr/bin/env bats

. $HOME/.cargo/env

@test "Ensure rustup binary is found in PATH" {
  run which rustup
  [ "$status" -eq 0 ]
}

@test "Ensure rustc stable binary is found in PATH" {
  run rustup run stable rustc --version
  [ "$status" -eq 0 ]
}

@test "Ensure rustc nightly binary is found in PATH" {
  run rustup run nightly rustc --version
  [ "$status" -eq 0 ]
}

@test "Ensure clippy binary is found in PATH" {
  run cargo clippy --version
  [ "$status" -eq 0 ]
}

# @test "Ensure racer binary is found in PATH" {
#   run which racer
#   [ "$status" -eq 0 ]
# }

@test "Ensure ripgrep binary is found in PATH" {
  run which rg
  [ "$status" -eq 0 ]
}

@test "Ensure rustfmt binary is found in PATH" {
  run which rustfmt
  [ "$status" -eq 0 ]
}

@test "Ensure tldr binary is found in PATH" {
  run which tldr
  [ "$status" -eq 0 ]
}
