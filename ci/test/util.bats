#!/usr/bin/env bats

@test "Ensure ag binary is found in PATH" {
  run which ag
  [ "$status" -eq 0 ]
}

@test "Ensure source-highlight binary is found in PATH" {
  run which source-highlight
  [ "$status" -eq 0 ]
}

@test "Ensure tldr binary is found in PATH" {
  run which tldr
  [ "$status" -eq 0 ]
}
