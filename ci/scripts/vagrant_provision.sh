#!/bin/bash

set -euo pipefail

export DEBIAN_FRONTEND=noninteractive

sudo apt-get update -qq && sudo apt-get install -y -qq ansible bats dbus dbus-x11 libglib2.0-bin sudo unzip zlib1g-dev
